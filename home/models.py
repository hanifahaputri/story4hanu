from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Year(models.Model):
    yearof = (("2019", "Maung (2019)"), ("2018", "Quanta (2018)"), ("2017", "Tarung (2017)"), ("2016", "Omega (2016)"), ("Alumni", "Alumni (2015—)"))
    year = models.CharField(choices=yearof, default="2019", null=True, max_length=6)
    
    def __str__(self):
        return self.year

class DataField(models.Model):
    # auto_increment_id = models.AutoField(primary_key=True)
    firstname = models.CharField(max_length=200, null=True)
    lastname = models.CharField(max_length=200, null=True)
    theyearof = (("2019", "Maung (2019)"), ("2018", "Quanta (2018)"), ("2017", "Tarung (2017)"), ("2016", "Omega (2016)"), ("Alumni", "Alumni (2015—)"))
    hobby = models.CharField(max_length=200, null=True)
    fnb = models.CharField(max_length=200, null=True)
    theyear = models.CharField(choices=theyearof, default="2019", null=True, max_length=6)

    def __str__(self):
        return self.firstname

# class Post(models.Model):
#     content = models.CharField(null=True)
#     published_data = models.DateTimeField(default=timezone.now)