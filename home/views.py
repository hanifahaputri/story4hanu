from .models import DataField
from .forms import DataFieldForm
from django.shortcuts import render
from . import forms

def home(request):
    return render(request, 'home/testhome.html')

def about(request):
    return render(request, 'home/testabout.html')

def contact(request):
    if request.method == 'POST':
        form = DataFieldForm(request.POST)
        if form.is_valid():
            # data_item = form.save(commit=False)
            # data_item.save()
            form.save()
            return render(request, 'home/testthx.html')

    else:
        form = DataFieldForm()
        return render(request, 'home/testcontact.html', {'form': form})

    
def portfolio(request):
    return render(request, 'home/testporto.html')

def friendlist(request):
    # form = DataFieldForm(request.POST)
    friendlist = DataField.objects.all()
    return render(request, 'home/testlist.html', {'friendlist': friendlist})

# def add_data(request):
#     if request.method == 'POST':
#         if form.is_valid():
#             blog_item = form.save(commit=False)
#             blog_item.save()
    
