from django.contrib import admin
from .models import DataField, Year

admin.site.register(DataField)
admin.site.register(Year)