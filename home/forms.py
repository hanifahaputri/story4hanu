from django.forms import ModelForm
from .models import DataField, Year

class DataFieldForm(ModelForm):
    class Meta:
        model = DataField
        fields = "__all__"